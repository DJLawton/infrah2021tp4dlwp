from myCode import MyUtils
from myCode.CustomExceptions import *
from myCode import PageBroker

class BitmapPage:

        def __init__(self, arrayOfUsedPages):
                self.__arrayOfUsedPages = arrayOfUsedPages

        def getUsedPages (self):
                return self.__arrayOfUsedPages

def parseFromHexDump(hexDump):
        nbPhysicalPages = MyUtils.hexToInt(MyUtils.extractSequence(PageBroker.getPage(0), 10, 2))
        arrayOfUsedPages = []
        index = 0
        #extractSequence takes length in bytes. A bit is used to represent each physical page.
        #"nbPhysicalPages / 8" because bits -> bytes
        digits = MyUtils.extractSequence(hexDump, 0, int(nbPhysicalPages / 8))
        for digit in digits:
                for binaryDigit in MyUtils.hexToBin(digit):
                        if int(binaryDigit):
                                arrayOfUsedPages.append(index)
                        index += 1
        return BitmapPage(arrayOfUsedPages)
