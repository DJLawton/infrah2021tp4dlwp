from myCode import MyUtils
from myCode import PageBroker
from myCode import PTEntry
from myCode import BitmapPage

class PTPage:

	def __init__(self, infoDict):
		self.__infoDict = infoDict

	def getInfo(self):
		return self.__infoDict

def parseFromHexDump(hexDump):
	octetsInDump = len(hexDump)/2
	entriesNb = int(octetsInDump / 64)
	bitMap = BitmapPage.parseFromHexDump(PageBroker.getPage(1))
	usedPages = bitMap.getUsedPages()

	ptDict = dict()
	for entryIndex in range(0, entriesNb):
		entryHexDump = MyUtils.extractSequence(hexDump, entryIndex*64, 64)
		if entryIndex in usedPages:
			ptDict[entryIndex] = PTEntry.parseFromHexDump(entryHexDump)
	return PTPage(ptDict)
