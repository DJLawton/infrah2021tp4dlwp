from myCode import MyUtils


def parseFromHexDump(hexDump):
	id = MyUtils.hexToInt(MyUtils.extractSequence(hexDump, 1, 1))
	swap = bool(int(MyUtils.hexToBin(MyUtils.extractSequence(hexDump, 0, 1))[0]))

	acl = [0]
	aclIndex = 2
	while MyUtils.extractSequence(hexDump, aclIndex, 1) != "00":
		acl.append(MyUtils.hexToInt(MyUtils.extractSequence(hexDump, aclIndex, 1)))
		aclIndex+=1

	ptEntryDict = dict()
	ptEntryDict['ACL'] = acl
	ptEntryDict['isSwappedOut'] = swap
	ptEntryDict['pageId'] = id
	ptEntryDict['pageLocation'] = id
	return ptEntryDict
