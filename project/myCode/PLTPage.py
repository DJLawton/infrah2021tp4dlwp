from myCode import MyUtils
from myCode import PLTEntry

class PLTPage:

	def __init__(self, arrayOfUsedEntries):
		self.__arrayOfUsedEntries = arrayOfUsedEntries

	def getUsedEntries(self):
		return self.__arrayOfUsedEntries

def parseFromHexDump(hexDump):
	octetsInDump = len(hexDump)/2
	nbEntries  = int(octetsInDump / 32)
	arrayOfUsedEntries = dict()
	for entryIndex in range(0, nbEntries):
		entryHexDump = MyUtils.extractSequence(hexDump, entryIndex*32, 32)
		entry = PLTEntry.parseFromHexDump(entryHexDump)
		if entry.isUsed():
			arrayOfUsedEntries[entryIndex] = entry.getUsedPages()
	return PLTPage(arrayOfUsedEntries)
