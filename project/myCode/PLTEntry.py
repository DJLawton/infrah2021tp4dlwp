from myCode import MyUtils

class PLTEntry:

	def __init__(self, isUsed, arrayOfUsedPages):
		self.__arrayOfUsedPages = arrayOfUsedPages
		self.__isUsed = isUsed

	def getUsedPages(self):
		return self.__arrayOfUsedPages

	def isUsed(self):
		return self.__isUsed

def parseFromHexDump(hexDump):
	#Take first bit, cast to int then cast to bool = True or False
	isUsed = bool(int(MyUtils.hexToBin(MyUtils.extractSequence(hexDump, 0, 1))[0]))
	arrayOfUsedPages = []
	arrayOfUsedPages.append(MyUtils.hexToInt(MyUtils.extractSequence(hexDump, 1, 1)))
	#Start from second byte
	for byteIndex in range(2, 32):
		pageNb = MyUtils.hexToInt(MyUtils.extractSequence(hexDump, byteIndex, 1))
		if pageNb:
			arrayOfUsedPages.append(pageNb)
		else:
			break #100% optimisation
		byteIndex+=1
	return PLTEntry(isUsed, arrayOfUsedPages)
