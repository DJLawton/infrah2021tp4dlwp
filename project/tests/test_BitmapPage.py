import unittest
import unittest.mock

from myCode import BitmapPage

class BitmapPageTests(unittest.TestCase):

        def test_usedPageGetter(self):
                arrayOfNumbers = [0,1,2,3,4,5,6,8,32]
                bitmapPage = BitmapPage.BitmapPage(arrayOfNumbers)
                bitmapInfo = bitmapPage.getUsedPages()

                self.assertEqual(bitmapInfo, arrayOfNumbers)

        @unittest.mock.patch('myCode.BitmapPage.PageBroker')
        def test_parseFromHex1(self, mock_utils):
                metaPageHead = "4D4C4D4C56534D4C0008008000000001" #128 physical pages
                mock_utils.getPage.return_value = metaPageHead
                hexdump = "FFE0000800200000000008000C000000" #exactly 16 bytes
                expected = [0,1,2,3,4,5,6,7,8,9,10,28,42,84,100,101]

                bitmapPage = BitmapPage.parseFromHexDump(hexdump)

                self.assertEqual(expected, bitmapPage.getUsedPages())

        @unittest.mock.patch('myCode.BitmapPage.PageBroker')
        def test_parseFromHex2(self, mock_utils):
                metaPageHead = "4D4C4D4C56534D4C0010004000000001" #128 physical pages
                mock_utils.getPage.return_value = metaPageHead
                hexdump = "FA10860080000000" #exactly 8 bytes
                expected = [0,1,2,3,4,6,11,16,21,22,32]

                bitmapPage = BitmapPage.parseFromHexDump(hexdump)

                self.assertEqual(expected, bitmapPage.getUsedPages())
