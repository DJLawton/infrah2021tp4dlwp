import unittest
import requests
import json
from myAPI import api

IP = "0.0.0.0"
PORT = "5555"
URL = "http://" + IP + ":" + PORT

class APITest(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		with open("./project/files/MemoryLayout1.vsml", "rb") as f:
			myFile = {'file':f}
			response = requests.post(URL + "/upload", files=myFile)
			if response.status_code != 200:
				print(response.content.decode())
				raise Exception("SetUpClass failed for APITest")


	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)

	def test_MetaRoute(self):
		response = requests.get(URL + "/metaInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['metaInfo']
		self.assertEqual("4D4C4D4C", map['magicNumber'])
		self.assertEqual("56534D4C", map['memoryLayoutType'])

		self.assertEqual(16, map['pageSize'])
		self.assertEqual(64, map['nbPhysicalPages'])
		self.assertEqual(0, map['nbSwapPages'])
		self.assertEqual(1, map['nbBitmapPages'])
		self.assertEqual(2, map['nbPTPages'])
		self.assertEqual(1, map['nbPLTPages'])

	def test_BitmapRoute(self):
		response = requests.get(URL + "/bitmapInfo")
		self.assertEqual(200, response.status_code)
		array = json.loads(response.content.decode('utf-8'))
		array = array['bitmapInfo']
		self.assertEqual(0, array[0])
		self.assertEqual(1, array[1])
		self.assertEqual(2, array[2])
		self.assertEqual(3, array[3])
		self.assertEqual(4, array[4])
		self.assertEqual(6, array[5])
		self.assertEqual(11, array[6])
		self.assertEqual(16, array[7])
		self.assertEqual(21, array[8])
		self.assertEqual(22, array[9])
		self.assertEqual(32, array[10])

	def test_ptRoute(self):
		response = requests.get(URL + "/ptInfo")
		self.assertEqual(200, response.status_code)
		dictionary = json.loads(response.content.decode('utf-8'))
		dictionary = dictionary['ptInfo']
		self.assertEqual(dictionary['0']['ACL'], [0])
		self.assertEqual(dictionary['0']['isSwappedOut'], False)
		self.assertEqual(dictionary['0']['pageId'], 0)
		self.assertEqual(dictionary['0']['pageLocation'], 0)
		self.assertEqual(dictionary['1']['ACL'], [0])
		self.assertEqual(dictionary['1']['isSwappedOut'], False)
		self.assertEqual(dictionary['1']['pageId'], 1)
		self.assertEqual(dictionary['1']['pageLocation'], 1)
		self.assertEqual(dictionary['2']['ACL'], [0])
		self.assertEqual(dictionary['2']['isSwappedOut'], False)
		self.assertEqual(dictionary['2']['pageId'], 2)
		self.assertEqual(dictionary['2']['pageLocation'], 2)
		self.assertEqual(dictionary['3']['ACL'], [0])
		self.assertEqual(dictionary['3']['isSwappedOut'], False)
		self.assertEqual(dictionary['3']['pageId'], 3)
		self.assertEqual(dictionary['3']['pageLocation'], 3)
		self.assertEqual(dictionary['4']['ACL'], [0])
		self.assertEqual(dictionary['4']['isSwappedOut'], False)
		self.assertEqual(dictionary['4']['pageId'], 4)
		self.assertEqual(dictionary['4']['pageLocation'], 4)
		self.assertEqual(dictionary['6']['ACL'], [0, 30])
		self.assertEqual(dictionary['6']['isSwappedOut'], False)
		self.assertEqual(dictionary['6']['pageId'], 6)
		self.assertEqual(dictionary['6']['pageLocation'], 6)
		self.assertEqual(dictionary['11']['ACL'], [0, 30])
		self.assertEqual(dictionary['11']['isSwappedOut'], True)
		self.assertEqual(dictionary['11']['pageId'], 11)
		self.assertEqual(dictionary['11']['pageLocation'], 11)
		self.assertEqual(dictionary['16']['ACL'], [0, 44, 8])
		self.assertEqual(dictionary['16']['isSwappedOut'], False)
		self.assertEqual(dictionary['16']['pageId'], 16)
		self.assertEqual(dictionary['16']['pageLocation'], 16)
		self.assertEqual(dictionary['21']['ACL'], [0, 8])
		self.assertEqual(dictionary['21']['isSwappedOut'], False)
		self.assertEqual(dictionary['21']['pageId'], 21)
		self.assertEqual(dictionary['21']['pageLocation'], 21)
		self.assertEqual(dictionary['22']['ACL'], [0, 30])
		self.assertEqual(dictionary['22']['isSwappedOut'], False)
		self.assertEqual(dictionary['22']['pageId'], 22)
		self.assertEqual(dictionary['22']['pageLocation'], 22)
		self.assertEqual(dictionary['32']['ACL'], [0, 8])
		self.assertEqual(dictionary['32']['isSwappedOut'], False)
		self.assertEqual(dictionary['32']['pageId'], 32)
		self.assertEqual(dictionary['32']['pageLocation'], 32)

	def test_pltRoute(self):
		response = requests.get(URL + "/pltInfo")
		self.assertEqual(200, response.status_code)
		dictionary = json.loads(response.content.decode('utf-8'))
		dictionary = dictionary['pltInfo']
		self.assertEqual(dictionary['0'], [0,1,2,3,4])
		self.assertEqual(dictionary['8'], [16,32,21])
		self.assertEqual(dictionary['30'], [22,6,11])
		self.assertEqual(dictionary['44'], [16])


if __name__ == "__main__":
	unittest.main()
