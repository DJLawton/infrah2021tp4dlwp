import unittest
import requests
import json
from myAPI import api

IP = "0.0.0.0"
PORT = "5555"
URL = "http://" + IP + ":" + PORT

class APITest(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		with open("./project/files/MemoryLayout2.vsml", "rb") as f:
			myFile = {'file':f}
			response = requests.post(URL + "/upload", files=myFile)
			if response.status_code != 200:
				print(response.content.decode())
				raise Exception("SetUpClass failed for APITest")


	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)

	def test_MetaRoute(self):
		response = requests.get(URL + "/metaInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['metaInfo']
		self.assertEqual("4D4C4D4C", map['magicNumber'])
		self.assertEqual("56534D4C", map['memoryLayoutType'])

		self.assertEqual(16, map['pageSize'])
		self.assertEqual(96, map['nbPhysicalPages'])
		self.assertEqual(0, map['nbSwapPages'])
		self.assertEqual(1, map['nbBitmapPages'])
		self.assertEqual(3, map['nbPTPages'])
		self.assertEqual(1, map['nbPLTPages'])

	def test_BitmapRoute(self):
		response = requests.get(URL + "/bitmapInfo")
		self.assertEqual(200, response.status_code)
		array = json.loads(response.content.decode('utf-8'))
		array = array['bitmapInfo']
		self.assertEqual(0, array[0])
		self.assertEqual(1, array[1])
		self.assertEqual(2, array[2])
		self.assertEqual(3, array[3])
		self.assertEqual(4, array[4])
		self.assertEqual(5, array[5])
		self.assertEqual(12, array[6])
		self.assertEqual(22, array[7])
		self.assertEqual(32, array[8])
		self.assertEqual(42, array[9])
		self.assertEqual(52, array[10])
		self.assertEqual(62, array[11])
		self.assertEqual(95, array[12])

	def test_ptRoute(self):
		response = requests.get(URL + "/ptInfo")
		self.assertEqual(200, response.status_code)
		dictionary = json.loads(response.content.decode('utf-8'))
		dictionary = dictionary['ptInfo']
		self.assertEqual(dictionary['0']['ACL'], [0])
		self.assertEqual(dictionary['0']['isSwappedOut'], False)
		self.assertEqual(dictionary['0']['pageId'], 0)
		self.assertEqual(dictionary['0']['pageLocation'], 0)
		self.assertEqual(dictionary['1']['ACL'], [0])
		self.assertEqual(dictionary['1']['isSwappedOut'], False)
		self.assertEqual(dictionary['1']['pageId'], 1)
		self.assertEqual(dictionary['1']['pageLocation'], 1)
		self.assertEqual(dictionary['2']['ACL'], [0])
		self.assertEqual(dictionary['2']['isSwappedOut'], False)
		self.assertEqual(dictionary['2']['pageId'], 2)
		self.assertEqual(dictionary['2']['pageLocation'], 2)
		self.assertEqual(dictionary['3']['ACL'], [0])
		self.assertEqual(dictionary['3']['isSwappedOut'], False)
		self.assertEqual(dictionary['3']['pageId'], 3)
		self.assertEqual(dictionary['3']['pageLocation'], 3)
		self.assertEqual(dictionary['4']['ACL'], [0])
		self.assertEqual(dictionary['4']['isSwappedOut'], False)
		self.assertEqual(dictionary['4']['pageId'], 4)
		self.assertEqual(dictionary['4']['pageLocation'], 4)
		self.assertEqual(dictionary['5']['ACL'], [0])
		self.assertEqual(dictionary['5']['isSwappedOut'], False)
		self.assertEqual(dictionary['5']['pageId'], 5)
		self.assertEqual(dictionary['5']['pageLocation'], 5)
		self.assertEqual(dictionary['12']['ACL'], [0, 2])
		self.assertEqual(dictionary['12']['isSwappedOut'], False)
		self.assertEqual(dictionary['12']['pageId'], 12)
		self.assertEqual(dictionary['12']['pageLocation'], 12)
		self.assertEqual(dictionary['22']['ACL'], [0, 2])
		self.assertEqual(dictionary['22']['isSwappedOut'], False)
		self.assertEqual(dictionary['22']['pageId'], 22)
		self.assertEqual(dictionary['22']['pageLocation'], 22)
		self.assertEqual(dictionary['32']['ACL'], [0, 2])
		self.assertEqual(dictionary['32']['isSwappedOut'], False)
		self.assertEqual(dictionary['32']['pageId'], 32)
		self.assertEqual(dictionary['32']['pageLocation'], 32)
		self.assertEqual(dictionary['42']['ACL'], [0, 2])
		self.assertEqual(dictionary['42']['isSwappedOut'], False)
		self.assertEqual(dictionary['42']['pageId'], 42)
		self.assertEqual(dictionary['42']['pageLocation'], 42)
		self.assertEqual(dictionary['52']['ACL'], [0, 2])
		self.assertEqual(dictionary['52']['isSwappedOut'], False)
		self.assertEqual(dictionary['52']['pageId'], 52)
		self.assertEqual(dictionary['52']['pageLocation'], 52)
		self.assertEqual(dictionary['62']['ACL'], [0, 2])
		self.assertEqual(dictionary['62']['isSwappedOut'], False)
		self.assertEqual(dictionary['62']['pageId'], 62)
		self.assertEqual(dictionary['62']['pageLocation'], 62)
		self.assertEqual(dictionary['95']['ACL'], [0, 3])
		self.assertEqual(dictionary['95']['isSwappedOut'], False)
		self.assertEqual(dictionary['95']['pageId'], 95)
		self.assertEqual(dictionary['95']['pageLocation'], 95)

	def test_pltRoute(self):
		response = requests.get(URL + "/pltInfo")
		self.assertEqual(200, response.status_code)
		dictionary = json.loads(response.content.decode('utf-8'))
		dictionary = dictionary['pltInfo']
		self.assertEqual(dictionary['0'], [0,1,2,3,4,5])
		self.assertEqual(dictionary['2'], [32,12,62,42,52,22])
		self.assertEqual(dictionary['3'], [95])


if __name__ == "__main__":
	unittest.main()
