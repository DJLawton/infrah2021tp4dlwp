docker stop tp4dlwp
docker container rm tp4dlwp
docker volume rm tp4dlwp_vol
docker image rm tp4dlwp_image
docker volume create --name tp4dlwp_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp4dlwp_image -f ./project/docker/Dockerfile .
docker run -d -p 5555:5555 --mount source=tp4dlwp_vol,target=/mnt/app/ --name tp4dlwp tp4dlwp_image
